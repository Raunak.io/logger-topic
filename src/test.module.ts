import { LoggerModule } from './logger.module';
import { TestService } from './test.service';
import { TestController } from './test.controller';
import { Module } from '@nestjs/common';

@Module({
  imports: [LoggerModule],
  controllers: [TestController],
  providers: [TestService],
})
export class TestModule {}
