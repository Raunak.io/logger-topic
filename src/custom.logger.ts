import { LoggerService, Logger, Injectable, Scope } from '@nestjs/common';


// @Injectable({scope: Scope.TRANSIENT}) // scope is used to configur metadata for this class
// we have used transient to ensure the unique instance of logger in each feature module
export class MyLogger implements LoggerService{


log(message:string):any{
console.log(`this is a log message -- ${message}`);
}
error(message:string,trace:string):any{
    console.log(`this is a error message -- ${message} and the trace -- ${trace}`);
}
warn(message:string):any{
    console.log(`this is a warn message -- ${message}`);
}
debug(message:string):any{
    console.log(`this is a debug log message -- ${message}`);
}
verbose(message:string):any{
    console.log(`this is a verbose message - ${message}`);
}

} // this one is a custom logger

@Injectable({scope: Scope.TRANSIENT})  // here transient means to make sure that all feature module are getting unique instance of logger
// extending built in logger
export class MySecondLogger extends Logger {

    log(message:string):any{
        super.log(message)
        console.log(`this is a log message -- ${message}`);
        }
        error(message:string,trace:string):any{
            super.error(message,trace)
            console.log(`this is a error message -- ${message} and the trace -- ${trace}`);
        }
        warn(message:string):any{
            super.warn(message)
            console.log(`this is a warn message -- ${message}`);
        }
        debug(message:string):any{
            super.debug(message)
            console.log(`this is a debug log message -- ${message}`);
        }
        verbose(message:string):any{
            super.verbose(message)
            console.log(`this is a verbose message - ${message}`);
        }

} // this one extends the logger