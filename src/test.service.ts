import { MyLogger,MySecondLogger } from './custom.logger';
import { Injectable } from '@nestjs/common';
@Injectable()

export class TestService{

constructor(private readonly myCustomLogger:MySecondLogger){
this.myCustomLogger.setContext('TestService');

}

    showGreeting():string{
        this.myCustomLogger.log('just for simple test logging');
        this.myCustomLogger.warn('just for simple warning  logging');
        return 'this is just a greeting message';
    }
}