import { MyLogger, MySecondLogger } from './custom.logger';
import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { WINSTON_MODULE_NEST_PROVIDER, WinstonModule } from 'nest-winston';

async function bootstrap() {
  const app = await NestFactory.create(
    AppModule,
    // {
    //   logger:WinstonModule.createLogger({}) // can also do this way
    // this way we don't need to import winstonModule in app module
    // }
    //    {
    //   logger: false,

    //   // logger: new MySecondLogger(),  // custom logger extending builtin logger
    //   // logger: new MyLogger(),  // custom logger
    //   // logger: console, // to see every thing in console
    //   // logger: ['warn'],
    // }//,{logger:false}
  );

  app.useLogger(app.get(WINSTON_MODULE_NEST_PROVIDER));
  // app.useLogger(app.get(MyLogger)); // get is used here to retrive singleton instance of my logger
  // app.useLogger(new MySecondLogger());
  await app.listen(3000);
}
bootstrap();
