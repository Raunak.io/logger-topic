import { MyLogger, MySecondLogger } from './custom.logger';
import { Module } from '@nestjs/common';

@Module({
 providers: [MyLogger, MySecondLogger],
  exports: [MySecondLogger, MyLogger],
})
export class LoggerModule {}
// seperate module for loggers 