import { TestModule } from './test.module';
import { LoggerModule } from './logger.module';
import { Module, Logger } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import {
  utilities as nestWinstonModuleUtilities,
  WinstonModule,
} from 'nest-winston';

import * as winston from 'winston';

import 'winston-daily-rotate-file';
import * as DailyRotateFile from 'winston-daily-rotate-file';

@Module({
  imports: [
    LoggerModule,
    TestModule,
    WinstonModule.forRoot({
      transports: [
        // new winston.transports.DailyRotateFile({
        //  new DailyRotateFileasasrotator({
        new DailyRotateFile({
          dirname: './logs',
          filename: 'winston.log.%DATE%',
          datePattern: 'YYYY-MM-DD-HH',
          zippedArchive: false,
          maxSize: '20m', // getting error while implementing this
          maxFiles: '1d',
          frequency:'1m'
        }),

        new winston.transports.Console({
          format: winston.format.combine(
            // this is for  formatting logger in console

            winston.format.timestamp(),
            nestWinstonModuleUtilities.format.nestLike(),
          ),
        }),
      ],
    }),
  ],
  controllers: [AppController],
  providers: [
    AppService,
    // Logger  //only use when importing module in main.ts
  ],
})
export class AppModule {}
