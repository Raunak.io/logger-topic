import { TestService } from './test.service';
import { Controller, Get, Inject, LoggerService } from '@nestjs/common';
import { WINSTON_MODULE_PROVIDER,WINSTON_MODULE_NEST_PROVIDER } from 'nest-winston';
import { Logger } from 'winston';
@Controller('test')
export class TestController {
  constructor(private readonly service: TestService ,
    // @Inject(Logger) private readonly anotherLogger:LoggerService // with second winston implementation
    // @Inject(WINSTON_MODULE_PROVIDER) private readonly winstonLogger:Logger
    @Inject(WINSTON_MODULE_NEST_PROVIDER) private readonly winstonLogger:LoggerService
    ) {}

  @Get()
  getGreet(): string {
    return this.service.showGreeting();
  }
}
